/*
	MongoDB - Query Operators
	Expand Queries in MongoDb using Query Operators
*/

/*
	Overview:
	-Definition
	-Importance

	types:
	1. Comparison Query Operators
		1.1 Greater Than
		1.2 Great than or equal to
		1.3 Less than
		1.4 Less than or equal to
		1.5 Not equal to
		1.6 In
	2.Evaluation Query Operator
		2.1 Regex
			2.1.1 Case sensitive query
			2.1.2 Case insensitive query
	3. Logical Query Operators
		3.1 OR
		3.2 AND

	Field Projection:
		1. Inclusion
		 	1.1 Returning specific fields in embeded documents
		 	1.2 Exception to the inclusiuon rule
		 	1.3 Slice operator
		 2. Exclusion
		 	2.1 Excluding specific field in embeded document
*/
/*
	What does Query Operator Mean?
	- A "Query" is a request for data from a database.
	- An "Operator" is a symbol that represents an action or a process
	- puttin them together, they mean the things that we can do on our queries using certain operators
*/
/*
	Query operators will enable us to create queries that can do more than what simple operators do. (We can do more than what we do in simple CRUD operations)
	-In CRUD operations, findOne using specific value inside its single or multiple parameters. When we know query operators ,we may look for records are mor specific
*/
// Comparison Query Operators
/*
	Includes:
		- Greater Than
		- Great than or equal to
		- Less than
		- Less than or equal to
		- Not equal to
		- In
*/
/*
	Greater Than "$gt"
	finds documents that have field number values greater than specific value

	Syntax:
		db.collectionName.find({field : {$gt : value}});
*/

db.users.find({age: {$gt : 76}});

/*
	Greater than or equal to "$gte"
	finds documents that have field number values greater than or equal to the specific value

	Syntax:
		db.collectionName.find({field : {$gte: value}});
*/

db.users.find({age: {$gte: 76}});

/*
	Less than "$lt"
	finds documents that have field number values less than the specific value

	Syntax:
		db.collectionName.find({field: {$lt: value}});
*/
db.users.find({age: {$lt: 65}});

/*
	Less than or equal to "$lte"
	finds documents that have field number values less than or equal to the specific value

	Syntax: 
		db.collectionName.find({field: {$lte: value}});
*/

db.users.find({age: {$lte: 65}});

/*
	Not equal to "$ne"
	finds documents that have field number values that are not equal to the specific value

	Syntax:
		db.collectionName.find({field: {$ne: value}});
*/

db.users.find({age: {$ne: 65}});

/*
	In operator "$in"
	-finds documents with specific match criteria on one field usign different values

	Syntax:
		db.collectionName.find({field : {$in: value}});
*/

db.users.find({lastName: {$in : ["Hawking", "Doe"]}});
db.users.find({courses : {$in : ["HTML", "React"]}});

//Evaluation Query Operators
// - Evaluation operators return data based on evaluations of either individual fields or the entire collection's documents.

/*
	Regex "$regex"
	is short for regular expressions
	-They are based on regular language
	RegEx is used for matching strings
	allows us to find documents that match a specific string pattern using regular expressions
*/

/*
	Case sensitive query

	Syntax:
		db.collectionName.find({field : {$regex: "string pattern"}})
*/

db.users.find({lastName: {$regex: "A"}});

/*
	Case insensitive query
	can run case-insensitivity queries by utilizing the "i" option

	Syntax:
		db.collectionName.find({field: {$regex : 'string pattern', $options: "$optionValue"}});
*/
db.users.find({lastName: {$regex: "A", $options: "$i"}});

db.users.find({firstName: {$regex: "e", $options: "$i"}});

// Logical Query Operators
/*
	OR operator "$or"
	-finds documents that match a single criterai from multple provided search criteria

	Syntax:
		db.collectionName.find({ $or: [{fieldA : valueA}, {fieldB : valueB}]});
*/

db.users.find({$or: [{firstName : "Neil"}, {age: "25"}]});
db.users.find({$or: [{firstName : "Neil"}, {age: {$gt: 30}}]});

/*
	END operator "$and"
	-Finds documents matching multiple criterai in a single field

	Syntax:
		db.collectionName.find({$and : [{fieldA : valueA}, {fieldB : valueB}]});
*/

db.users.find({$and : [{age : {$ne: 82}}, {age : {$ne: 76}}]});

db.users.find({
	$and: 
	[{firstName: {$regex: "e"},$options: "$i"}, 
	{age :{$lte: 30}}]
});

db.users.find({$and: [{firstName: {$regex: "e"}}, {age: {$lte: 30}}]})

//Field Projection
/*
	By default, MongoDB returns the whole document especially when dealing with complex documents,
	but sometimes it isn't helpful to view the whole document, especially when dealing with more complex ones
	to help with the readability of the values returned (or sometimes, because of security reasons), we include or exclude some fields
	we project our selected field
*/

/*
	Inclusion
		-allows us to include or add specific fields only when retrieving documents
		- value denoted is "1" to indicate the field is being included
		-we cannot do exclusion on field that uses inclusion projection

		Syntax:
			db.collectionName.find({criteria}, {field: 1})
*/

db.users.find({firstName: "Jane"})

db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName:1,
	"contact.phone": 1
}
);

/*
	Returning specific fields in embedded documents
	-the double quotations are important.
*/
db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName:1,
	"contact.phone": 1
}
);

/*
	Exception to the inclusion rule: Suppressing the ID field
	-allows us to exclude the "_id" field when retreiving document
	when using field projection, field inclusion and exclusion may not be used at the same time.
	"_id" field is the only exception to this rule

	Syntax:
		db.collectionName.find({criteria}, {_id: 0})
*/

db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName:1,
	contact: 1,
	_id: 0
}
);

/*
	Slice Operator
	"$slice" allows us to retrieve only 1 element that matches the search criteria
*/

db.users.insert({
	nameArr: [
		{
			nameA: "Juan"
		},
		{
			nameB: "Tamad"
		}
	]
});

db.users.find({
	nameArr:{
		nameA: "Juan"
	}
});

db.users.find(
	{ nameArr: 
		{ 
			nameA: "Juan" 
		} 
	}, 
	{ nameArr: 
		{ $slice: 1 } 
	}
);

db.users.find(
	{
		firstName:
		{$regex: "s", $options: "$i"}
	},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);

/*
	Exclusion
	-Allows us to exclude or remove specific fields when retrieving documents
	the value provided is zero to denote that the field is being included

	Syntax:
		db.collectionName.find({criteria}, field: 0);
*/

db.users.find(
{
	firstName: "Jane"
},
{
	contact: 0,
	department: 0
}
);

/*
	Excluding/Supressing specific fields in embeded documents
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone" : 0
	}
);